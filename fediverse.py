#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pdb
from six.moves import urllib
#import datetime
from datetime import datetime
import pytz
from subprocess import call
from mastodon import Mastodon
import time
import threading
import csv
import os
import json
import time
import signal
import sys
import os.path        # For checking whether secrets file exists
import requests       # For doing the web stuff, dummy!
import operator
import calendar
import psycopg2
from importlib import reload
from decimal import *
getcontext().prec = 2

reload(sys)

###############################################################################
# INITIALISATION
###############################################################################

do_upload = True
# Run without uploading, if specified
if '--no-upload' in sys.argv:
    do_upload = False

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):    
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

# Load secrets from secrets file
secrets_filepath = "secrets/secrets.txt"
uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

# Load configuration from config file
config_filepath = "config.txt"
mastodon_hostname = get_parameter("mastodon_hostname", config_filepath) # E.g., mastodon.social
mastodon_db = get_parameter("mastodon_db", config_filepath) # E.g., mastodon_production
mastodon_db_user = get_parameter("mastodon_db_user", config_filepath) # E.g., mastodon
fediverse_db = get_parameter("fediverse_db", config_filepath) # E.g., fediverse_prod
fediverse_db_user = get_parameter("fediverse_db_user", config_filepath) # E.g., mastodon

# Initialise Mastodon API
mastodon = Mastodon(
    client_id = uc_client_id,
    client_secret = uc_client_secret,
    access_token = uc_access_token,
    api_base_url = 'https://' + mastodon_hostname,
)

# Initialise access headers
headers={ 'Authorization': 'Bearer %s'%uc_access_token }

###############################################################################
# 
###############################################################################

sslerror = 0
connectionerror = 0
timeouterror= 0
mastodont = 0
pleroma = 0
gnusocial = 0
zap = 0
plume = 0
hubzilla = 0
misskey = 0
prismo = 0
osada = 0
groundpolis = 0
ganggo = 0
squs = 0
peertube = 0
friendica = 0
pixelfed = 0
others = 0
noresponse = 0

users_pl = 0
users_pl_total = 0
users_mast = 0
users_mast_total = 0
users_gs = 0
users_gs_total = 0
users_zap = 0
users_zap_total = 0
users_plume = 0
users_plume_total = 0
users_hubzilla = 0
users_hubzilla_total = 0
users_misskey = 0
users_misskey_total = 0
users_prismo = 0
users_prismo_total = 0
users_osada = 0
users_osada_total = 0
users_gpolis = 0
users_gpolis_total = 0
users_ggg = 0
users_ggg_total = 0
users_squs = 0
users_squs_total = 0
users_peertube = 0
users_peertube_total = 0
users_friendica = 0
users_friendica_total = 0
users_pixelfed = 0
users_pixelfed_total = 0
users_total = 0

servers_total = 0
visited = 0
server_soft = ""

tz = pytz.timezone('Europe/Madrid')
ara = datetime.now(tz)

try:

  conn = None
  conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")

  cur = conn.cursor()

  ### get mastodon_hostname federated servers

  cur.execute("select distinct domain from accounts where domain is not null")

  federated_servers = []

  for row in cur:

    federated_servers.append(row[0]) ## guarda els servidors federats en l'array federated_servers

  ### per a obtenir els servidors federats bloquejats #####################

  cur.execute("select * from domain_blocks where severity = '1'")

  blocked_servers = []

  for row in cur:

    blocked_servers.append(row[1]) ## guarda els servidors bloquejats en l'array blocked_servers

  i = 0

  while i < len(blocked_servers):

    delete=federated_servers.index(blocked_servers[i])
    federated_servers[delete:delete+1] = []
    i += 1

  # tancar la connexió amb la base de dades
  cur.close()

except (Exception, psycopg2.DatabaseError) as error:

  print(error)

finally:

  if conn is not None:

    conn.close()

  ###########################################################################

i = 0

while i < len(federated_servers):
    
  users = 0 
  instances = 0
  toots = 0
  server_soft = ""

  print(str(i) + " de " + str(len(federated_servers)) + " - Checking server: " + str(federated_servers[i]))
  print("\n")
  print("############################################################################")
  print("Servers:") 
  print("sslerror: " + str(sslerror) + ", connerror: " + str(connectionerror) + ", timeout err: " + str(timeouterror) + ", Mastodon: " + str(mastodont) + ", Pleroma: " + str(pleroma))
  print("GNU Social: " + str(gnusocial) + ", Zap: " + str(zap) + ", Plume: " + str(plume) + ", Hubzilla: " + str(hubzilla) + ", Misskey: " + str(misskey) + ", Prismo: " + str(prismo))
  print("Osada: " + str(osada) + ", Groundpolis: " + str(groundpolis) + ", Ganggo: " + str(ganggo) + ", Squs: " + str(squs) + ", Peertube: " + str(peertube) + ", Friendica: " + str(friendica))
  print("Pixelfed: " + str(pixelfed) + " Others: " + str(others))
  print("No response: " + str(noresponse))
  print("############################################################################")

  try:

    res = requests.get('https://' + federated_servers[i] + '/api/v1/instance?',timeout=3)

    if (res.ok):

      if res.json()['stats']['user_count'] != "":

        instances = res.json()['stats']['domain_count']

        nodeinfo = requests.get('https://' + federated_servers[i] + '/nodeinfo/2.0.json?',timeout=3)
        check_pleroma = "pleroma" in nodeinfo.text.lower()

        if nodeinfo.status_code == 200 and check_pleroma == True:

          pleroma = pleroma +1
          print("Pleroma servers: " + str(pleroma))
          users_pl = nodeinfo.json()['usage']['users']['total']
          users_pl_total = users_pl_total + users_pl
          users = users_pl
          toots = res.json()['stats']['status_count']
          server_soft = "Pleroma"
          print(federated_servers[i], users_pl, instances, toots)

        else:

          mastodont = mastodont + 1
          print("Mastodon servers: " + str(mastodont))
          server_soft = "Mastodon"
          users_mast = res.json()['stats']['user_count']
          users_mast_total = users_mast_total + users_mast
          users = users_mast
          toots = res.json()['stats']['status_count']
          print(federated_servers[i], users_mast, instances, toots)

        users_total = users_mast_total + users_pl_total + users_gs_total + users_zap_total + users_plume_total + users_hubzilla_total + users_misskey_total + users_prismo_total + users_osada_total + users_gpolis_total + users_ggg_total + users_squs_total + users_peertube_total + users_friendica_total + users_pixelfed_total

        print("\n") 

        #####################################################################################################################################

        insert_line = """INSERT INTO federation(server, users, fed_servers, toots, updated_at, software)
                               VALUES(%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"""
        conn = None

        try:
 
          conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

          cur = conn.cursor()

          # executem INSERT
          cur.execute(insert_line, (federated_servers[i], users, instances, toots, ara, server_soft))

          # executem UPDATE
          cur.execute("UPDATE federation SET users=(%s), fed_servers=(%s), toots=(%s), updated_at=(%s), software=(%s) where server=(%s)",
                     (users, instances, toots, ara, server_soft, federated_servers[i]))

          # salvar els canvis en la base de dades
          conn.commit()

          # i tancar la connexió amb la base de dades
          cur.close()

        except (Exception, psycopg2.DatabaseError) as error:
          print(error)

        finally:
          if conn is not None:
            conn.close()

        i += 1
        continue

  except KeyError:

    print("KeyError!")
    others = others + 1
    print("Server (KeyError): " + federated_servers[i])
    print("Others servers: " + str(others))
    print("\n")
    i += 1

  except ValueError as verr:

    print("ValueError!")
    print(verr)
    others = others + 1
    print("Server (ValeuError): " + federated_servers[i])
    print("Others servers: " + str(others))
    print("\n")
    i += 1

  except requests.exceptions.SSLError as errssl:
  
    sslerror = sslerror + 1
    print(federated_servers[i] + "** ssl error: " + str(sslerror))
    print("\n")
    # afegir servidor amb ssl error a la taula servidors_sslerror ###########################

    insert_line = """INSERT INTO ssl_error_servers(server, added, updated_at, days)
                               VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"""
    conn = None

    try:

      conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

      cur = conn.cursor()

      # executem INSERT
      cur.execute(insert_line, (federated_servers[i], ara, ara, ""))

      # executem SELECT
      cur.execute("SELECT added FROM ssl_error_servers where server=(%s)", (federated_servers[i],))

      row = cur.fetchone()
      delta = ara-row[0]

      # execute UPDATE
      cur.execute("UPDATE ssl_error_servers SET updated_at=(%s), days=(%s) where server=(%s)", (ara, delta, federated_servers[i]))

      # salvar els canvis en la base de dades
      conn.commit()

      # i tancar la connexió amb la base de dades
      cur.close()

    except (Exception, psycopg2.DatabaseError) as error:

      print(error)

    finally:

      if conn is not None:

        conn.close()

    ########################################################################################

    i += 1

  except requests.exceptions.HTTPError as errh:

    print("2 - Http Error:",errh)
    i += 1

  except requests.exceptions.ConnectionError as errc:

    #print ("3 - Error Connecting:",errc)
    connectionerror = connectionerror + 1
    print("#####################################################################")
    print(federated_servers[i] + " ** connection error: " + str(connectionerror))
    print("#####################################################################")
    print("\n")
    # afegir servidor amv connection error a la taula servidors_conerror ###################

    insert_line = """INSERT INTO conn_error_servers(server, added, updated_at, days)
                               VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"""
    conn = None

    try:

      conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

      cur = conn.cursor()

      # executem INSERT
      cur.execute(insert_line, (federated_servers[i], ara, ara, ""))

      # executem SELECT
      cur.execute("SELECT added FROM conn_error_servers where server=(%s)", (federated_servers[i],))

      row = cur.fetchone()
      delta = ara-row[0]

      # execute UPDATE
      cur.execute("UPDATE conn_error_servers SET updated_at=(%s), days=(%s) where server=(%s)", (ara, delta, federated_servers[i]))

      # salvar els canvis en la base de dades
      conn.commit()

      # i tancar la connexió amb la base de dades
      cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
      print(error)

    finally:

      if conn is not None:

        conn.close()

    ########################################################################################

    i += 1

  except requests.exceptions.Timeout as errt:

    #print ("4 - Timeout Error:",errt)
    timeouterror = timeouterror + 1
    print(federated_servers[i] + " ** timeout error: " + str(timeouterror))
    print("\n")
    # afegir el servidor amb timeout error a la taula servidors_timeouterror ###############

    insert_line = """INSERT INTO time_out_servers(server, added, updated_at, days)
                               VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"""
    conn = None

    try:

      conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

      cur = conn.cursor()

      # executem INSERT
      cur.execute(insert_line, (federated_servers[i], ara, ara, ""))

      # executem SELECT
      cur.execute("SELECT added FROM time_out_servers where server=(%s)", (federated_servers[i],))

      row = cur.fetchone()
      delta = ara-row[0]

      # execute UPDATE
      cur.execute("UPDATE time_out_servers SET updated_at=(%s), days=(%s) where server=(%s)", (ara, delta, federated_servers[i]))

      # salvar els canvis en la base de dades
      conn.commit()

      # i tancar la connexió amb la base de dades
      cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
      print(error)

    finally:
      if conn is not None:
        conn.close()

    ########################################################################################

    i += 1

  except requests.exceptions.RequestException as err:

    print("5 - OOps: Something Else",err)
    i += 1

  else:
    #pdb.set_trace()      
    try:
       
      others_nodeinfo = requests.get('https://' + federated_servers[i] + '/nodeinfo/2.0?',timeout=3)
      gs_nodeinfo = requests.get('https://' + federated_servers[i] + '/main/nodeinfo/2.0?',timeout=3)
      nodeinfo = requests.get('https://' + federated_servers[i] + '/nodeinfo/2.0.json?',timeout=3)
      statusnet = requests.get('https://' + federated_servers[i] + '/api/statusnet/config?',timeout=3)
      px_nodeinfo = requests.get('https://' + federated_servers[i] + '/api/nodeinfo/2.0.json?',timeout=3) 

      if others_nodeinfo.ok == False and gs_nodeinfo.ok == False and nodeinfo.ok == False and statusnet.ok == False and px_nodeinfo.ok == False:

        noresponse = noresponse + 1
        print("No response " + str(noresponse))
        print("\n")
        print(federated_servers[i] + " ** no response: " + str(noresponse))

        # afegir servidor sense resposta a la taula servidors_no_responen ###################

        insert_line = """INSERT INTO no_response_servers(server, added, updated_at, days)
                               VALUES(%s,%s,%s, %s) ON CONFLICT DO NOTHING"""
        conn = None

        try:

          conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

          cur = conn.cursor()

          # executem INSERT
          cur.execute(insert_line, (federated_servers[i], ara, ara, ""))

          # executem SELECT
          cur.execute("SELECT added FROM no_response_servers where server=(%s)", (federated_servers[i],))

          row = cur.fetchone()
          delta = ara-row[0]

          # execute UPDATE
          cur.execute("UPDATE no_response_servers SET updated_at=(%s), days=(%s) where server=(%s)", (ara, delta, federated_servers[i]))

          # salvar els canvis en la base de dades
          conn.commit()

          # i tancar la connexió amb la base de dades
          cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

          print(error)

        finally:

          if conn is not None:
            conn.close()

        i += 1

      else:
    
        check_peertube = "peertube" in nodeinfo.text
        check_zap = "zap" in others_nodeinfo.text
        check_plume = "plume" in others_nodeinfo.text
        check_hubzilla = "hubzilla" in others_nodeinfo.text
        check_misskey = "misskey" in others_nodeinfo.text
        check_prismo = "prismo" in others_nodeinfo.text
        check_osada = "osada" in others_nodeinfo.text
        check_groundpolis = "groundpolis" in others_nodeinfo.text
        check_ganggo = "ganggo" in others_nodeinfo.text
        check_squs = "squs" in others_nodeinfo.text
        check_gnusocial = "gnusocial" in gs_nodeinfo.text
        check_friendica = "friendica" in statusnet.text
        check_pixelfed = "pixelfed" in px_nodeinfo.text
        #if federated_servers[i] == "msk.kirigakure.net":
          #pdb.set_trace()  

        if check_peertube == True or check_zap == True or check_plume == True or check_hubzilla == True or check_misskey == True or check_prismo == True or check_osada == True or check_groundpolis == True or check_ganggo == True or check_squs == True or check_gnusocial == True or check_friendica == True or check_pixelfed == True:

          if nodeinfo.ok == True and check_peertube == True:

            peertube = peertube + 1
            print("Peertube servers: " + str(peertube))
            users_peertube = nodeinfo.json()['usage']['users']['total']
            users_peertube_total = users_peertube_total + users_peertube
            users = users_peertube
            instances = 0
            toots = nodeinfo.json()['usage']['localPosts']
            server_soft = "Peertube"
            print(federated_servers[i], users_peertube, instances, toots)          

          if others_nodeinfo.ok == True and check_zap == True:
 
            zap = zap +1
            print("Zap servers: " + str(zap))
            users_zap = others_nodeinfo.json()['usage']['users']['total']
            users_zap_total = users_zap_total + users_zap
            users = users_zap
            toots = others_nodeinfo.json()['usage']['localPosts']
            instances = 0
            server_soft = "Zap"
            print(federated_servers[i], users_zap, instances, toots)
            print("\n")

          if others_nodeinfo.ok == True and check_plume == True:

            plume = plume + 1
            print("Plume servers: " + str(plume))
            users_plume = others_nodeinfo.json()['usage']['users']['total']
            users_plume_total = users_plume_total + users_plume
            users = users_plume
            toots = others_nodeinfo.json()['usage']['localPosts']
            instances = 0
            server_soft = "Plume"
            print(federated_servers[i], users_plume, instances, toots)
            print("\n")

          if others_nodeinfo.ok == True and check_hubzilla == True:

            hubzilla = hubzilla + 1
            print("Hubzilla servers: " + str(hubzilla))
            users_hubzilla = others_nodeinfo.json()['usage']['users']['total']
            users_hubzilla_total = users_hubzilla_total + users_hubzilla
            users = users_hubzilla
            toots = others_nodeinfo.json()['usage']['localPosts']
            instances = 0
            server_soft = "Hubzilla"
            print(federated_servers[i], users_hubzilla, instances, toots)
            print("\n")

          if others_nodeinfo.ok == True and check_misskey == True:
  
            misskey = misskey + 1
            print("Misskey servers: " + str(misskey))
            users_misskey = 0
            users_misskey_total = users_misskey_total + users_misskey
            users = users_misskey
            toots = 0  
            instances = 0
            server_soft = "Misskey"
            print(federated_servers[i], users_misskey, instances, toots)
            print("\n")

          if others_nodeinfo.ok == True and check_prismo == True:

            prismo = prismo + 1
            print("Prismo servers: " + str(prismo))
            users_prismo = others_nodeinfo.json()['usage']['users']['total']
            users_prismo_total = users_prismo_total + users_prismo
            users = users_prismo
            toots = others_nodeinfo.json()['usage']['localPosts']
            instances = 0
            server_soft = "Prismo"
            print(federated_servers[i], users_prismo, instances, toots)
            print("\n")

          if others_nodeinfo.ok == True and check_osada == True:

            osada = osada + 1
            print("Osada servers: " + str(osada))
            users_osada = others_nodeinfo.json()['usage']['users']['total']
            users_osada_total = users_osada_total + users_osada
            users = users_osada
            toots = others_nodeinfo.json()['usage']['localPosts']
            instances = 0
            server_soft = "Osada"
            print(federated_servers[i], users_osada, instances, toots)
            print("\n")

          if others_nodeinfo.ok == True and check_groundpolis == True:

            groundpolis = groundpolis + 1
            print("Groundpolis servers: " + str(groundpolis))
            users_gpolis = others_nodeinfo.json()['usage']['users']['total']
            users_gpolis_total = users_gpolis_total + users_gpolis
            users = users_gpolis
            toots = others_nodeinfo.json()['usage']['localPosts']
            instances = 0
            server_soft = "Groundpolis"
            print(federated_servers[i], users_gpolis, instances, toots)
            print("\n")

          if others_nodeinfo.ok == True and check_ganggo == True:

            ganggo = ganggo + 1
            print("Ganggo servers: " + str(ganggo))
            users_ggg = others_nodeinfo.json()['usage']['users']['total']
            users_ggg_total = users_ggg_total + users_ggg
            users = users_ggg
            toots = others_nodeinfo.json()['usage']['localPosts']
            instances = 0
            server_soft = "Ganggo"
            print(federated_servers[i], users_ggg, instances, toots)
            print("\n")

          if others_nodeinfo.ok == True and check_squs == True:

            squs = squs + 1
            print("Squs servers: " + str(squs))
            users_squs = others_nodeinfo.json()['usage']['users']['total']
            users_squs_total = users_squs_total + users_squs
            users = users_squs
            toots = others_nodeinfo.json()['usage']['localPosts']
            instances = 0
            server_soft = "Squs"
            print(federated_servers[i], users_squs, instances, toots)
            print("\n")

          if gs_nodeinfo.ok == True and check_gnusocial == True:
 
            gnusocial = gnusocial +1
            print("GNU Social servers: " + str(gnusocial))
            users_gs = gs_nodeinfo.json()['usage']['users']['total']
            users_gs_total = users_gs_total + users_gs
            users = users_gs
            toots = gs_nodeinfo.json()['usage']['localPosts']
            instances = 0
            server_soft = "GNU Social"
            print(federated_servers[i], users_gs, instances, toots)
            print("\n")

          if statusnet.ok == True and check_friendica == True: #statusnet.json()['site']['friendica']['FRIENDICA_PLATFORM'] == "Friendica":

            friendica = friendica + 1
            print("Friendica servers: " + str(friendica))
            #users_friendica = res.json()['usage']['users']['total']
            users_friendica = 0
            users_friendica_total = users_friendica_total + users_friendica
            users = users_friendica
            instances = 0
            #toots = statusnet.json()['usage']['localPosts']
            toots = 0
            server_soft = "Friendica"
            print(federated_servers[i], users_friendica, instances, toots)
            print("\n")

          if px_nodeinfo.ok == True and check_pixelfed == True:

            pixelfed = pixelfed + 1
            print("##########################################################")
            print("Pixelfed servers: " + str(pixelfed))
            users_pixelfed = px_nodeinfo.json()['usage']['users']['total']
            users_pixelfed_total = users_pixelfed_total + users_pixelfed
            users = users_pixelfed
            instances = 0
            toots = px_nodeinfo.json()['usage']['localPosts']
            server_soft = "Pixelfed"
            print(federated_servers[i], users_pixelfed, instances, toots)
            print("##########################################################")
            print("\n")
  

      if others_nodeinfo.ok or gs_nodeinfo.ok or nodeinfo.ok or statusnet.ok or px_nodeinfo.ok:

        users_total = users_mast_total + users_pl_total + users_gs_total + users_zap_total + users_hubzilla_total + users_misskey_total + users_plume_total + users_prismo_total + users_osada_total + users_gpolis_total + users_ggg_total + users_squs_total + users_peertube_total + users_friendica_total + users_pixelfed_total

        insert_line = """INSERT INTO federation(server, users, fed_servers, toots, updated_at, software)
                                                         VALUES(%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"""
        conn = None

        try:
 
          conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

          cur = conn.cursor()

          # executem INSERT
          cur.execute(insert_line, (federated_servers[i], users, instances, toots, ara, server_soft))

          # executem UPDATE
          cur.execute("UPDATE federation SET users=(%s), fed_servers=(%s), toots=(%s), updated_at=(%s), software=(%s) where server=(%s)",
                       (users, instances, toots, ara, server_soft, federated_servers[i]))

          # salvar els canvis en la base de dades
          conn.commit()

          # i tancar la connexió amb la base de dades
          cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

          print(error)

        finally:

          if conn is not None:

            conn.close()

        i += 1

    except KeyError:

      print("KeyError!")
      others = others + 1
      print("Server (KeyError): " + federated_servers[i])
      print("Others servers: " + str(others))
      print("\n")
      i += 1

    except ValueError as verr:

      print("ValueError!")
      print(verr)
      others = others + 1
      print("Server (ValueError): " + federated_servers[i])
      print("Others servers: " + str(others))
      print("\n")
      i += 1

    except requests.exceptions.ConnectionError as errc:

      #print ("3 - Error Connecting:",errc)
      connectionerror = connectionerror + 1
      print(federated_servers[i] + " ** connection error: " + str(connectionerror))
      print("\n")
      # afegir servidor amv connection error a la taula servidors_conerror ###################

      insert_line = """INSERT INTO conn_error_servers(server, added, updated_at, days)
                               VALUES(%s,%s,%s, %s) ON CONFLICT DO NOTHING"""
      conn = None

      try:

        conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        # executem INSERT
        cur.execute(insert_line, (federated_servers[i], ara, ara, ""))

        # executem SELECT
        cur.execute("SELECT added FROM conn_error_servers where server=(%s)", (federated_servers[i],))

        row = cur.fetchone()
        delta = ara-row[0]

        # execute UPDATE
        cur.execute("UPDATE conn_error_servers SET updated_at=(%s), days=(%s) where server=(%s)", (ara, delta, federated_servers[i]))

        # salvar els canvis en la base de dades
        conn.commit()

        # i tancar la connexió amb la base de dades
        cur.close()

      except (Exception, psycopg2.DatabaseError) as error:

        print(error)

      finally:

        if conn is not None:

          conn.close()

      ########################################################################################

      i += 1

    except requests.exceptions.Timeout as errt:

      #print ("4 - Timeout Error:",errt)
      timeouterror = timeouterror + 1
      print(federated_servers[i] + " ** timeout error: " + str(timeouterror))
      print("\n")
      # afegir el servidor amb timeout error a la taula servidors_timeouterror ###############

      insert_line = """INSERT INTO time_out_servers(server, added, updated_at, days)
                               VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"""
      conn = None

      try:

        conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        # executem INSERT
        cur.execute(insert_line, (federated_servers[i], ara, ara, ""))

        # executem SELECT
        cur.execute("SELECT added FROM time_out_servers where server=(%s)", (federated_servers[i],))

        row = cur.fetchone()
        delta = ara-row[0]

        # execute UPDATE
        cur.execute("UPDATE time_out_servers SET updated_at=(%s), days=(%s) where server=(%s)", (ara, delta, federated_servers[i]))

        # salvar els canvis en la base de dades
        conn.commit()

        # i tancar la connexió amb la base de dades
        cur.close()

      except (Exception, psycopg2.DatabaseError) as error:

        print(error)

      finally:

        if conn is not None:

          conn.close()

      i += 1
  
  finally:
   
    #print stats_federats

    ###############################################################################################
    # esborrar els servidors que han tornat a la vida                                             #
    ###############################################################################################

    if i == len(federated_servers):

      conn = None

      try:

        conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        # delete back on life servers
        cur.execute("DELETE from ssl_error_servers where updated_at < %s OR updated_at is null", (ara,))

        # delete back on life servers
        cur.execute("DELETE from conn_error_servers where updated_at < %s OR updated_at is null", (ara,))

        # delete back on life servers
        cur.execute("DELETE from time_out_servers where updated_at < %s OR updated_at is null", (ara,))

        # salvar els canvis en la base de dades
        conn.commit()

        # i tancar la connexió amb la base de dades
        cur.close()

      except (Exception, psycopg2.DatabaseError) as error:

        print(error)

      finally:

        if conn is not None:

          conn.close()

      ########################### esborra de la taula federació els servidors no actius

      conn = None

      try:

        conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        # delete no active servers
        cur.execute("DELETE from federation where updated_at < %s OR updated_at is null", (ara,))

        # salvar els canvis en la base de dades
        conn.commit()

        # i tancar la connexió amb la base de dades
        cur.close()

      except (Exception, psycopg2.DatabaseError) as error:

        print(error)

      finally:

        if conn is not None:

          conn.close()

    ################################################################################################
    # obtenim els valors de la verificació anterior
    ################################################################################################

    #pdb.set_trace()
    if i == len(federated_servers):
      #pdb.set_trace()
      servers_total = mastodont + pleroma + gnusocial + zap + plume + hubzilla + misskey + prismo + osada + groundpolis + ganggo + squs + peertube + friendica + pixelfed + others
      visited = len(federated_servers)

      select = """select visited, mastodon, others, no_response, ssl_error, timeout_error, connection_error, total_servers, total_users, pleroma, masto_users,
                       pleroma_users, gnusocial, gnusocial_users, zap, zap_users, plume, plume_users, hubzilla, hubzilla_users, misskey, misskey_users, prismo, prismo_users, osada, osada_users,
                       groundpolis, groundpolis_users, ganggo, ganggo_users, squs, squs_users, peertube, peertube_users, friendica, friendica_users, pixelfed, pixelfed_users from federated_servers 
                       order by datetime desc limit 1;"""
      try:

        conn = None
        conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        cur.execute(select)

        row = cur.fetchone()

        visited_before = row[0]
        mastodont_before = row[1]
        others_before = row[2]
        noresponse_before = row[3]
        sslerror_before = row[4]
        timeout_error_before = row[5]
        connerror_before = row[6]
        serv_total_before = row[7]
        users_total_before = row[8]
        pleroma_before = row[9]
        users_mastodont_before = row[10]
        users_pleroma_before = row[11]
        gnusocial_before = row[12]
        users_gnusocial_before = row[13]
        zap_before = row[14]
        users_zap_before = row[15]
        plume_before = row[16]
        users_plume_before = row[17]
        hubzilla_before = row[18]
        users_hubzilla_before = row[19]
        misskey_before = row[20]
        users_misskey_before = row[21]
        prismo_before = row[22]
        users_prismo_before = row[23]
        osada_before = row[24]
        users_osada_before = row[25]
        groundpolis_before = row[26]
        users_groundpolis_before = row[27]
        ganggo_before = row[28]
        users_ganggo_before = row[29]
        squs_before = row[30]
        users_squs_before = row[31]
        peertube_before = row[32]
        users_peertube_before = row[33]
        friendica_before = row[34]
        users_friendica_before = row[35]
        pixelfed_before = row[36]
        users_pixelfed_before = row[37]

        # tancar la connexió amb la base de dades
        cur.close()

        evo_visited = visited - visited_before
        evo_mastodont = mastodont - mastodont_before
        evo_others = others - others_before
        evo_noresponse = noresponse - noresponse_before
        evo_sslerror = sslerror - sslerror_before
        evo_timeout_error = timeouterror - timeout_error_before
        evo_connerror = connectionerror - connerror_before
        evo_serv_total = servers_total - serv_total_before
        evo_users_total = users_total - users_total_before
        evo_pleroma = pleroma - pleroma_before
        evo_users_mast = users_mast_total - users_mastodont_before
        evo_users_pl = users_pl_total - users_pleroma_before
        evo_gnusocial = gnusocial - gnusocial_before
        evo_users_gs = users_gs_total - users_gnusocial_before
        evo_zap = zap - zap_before
        evo_users_zap = users_zap_total - users_zap_before
        evo_plume = plume - plume_before
        evo_users_plume = users_plume_total - users_plume_before
        evo_hubzilla = hubzilla - hubzilla_before
        evo_users_hub = users_hubzilla_total - users_hubzilla_before
        evo_misskey = misskey - misskey_before
        evo_users_misskey = users_misskey_total - users_misskey_before
        evo_prismo = prismo - prismo_before
        evo_users_prismo = users_prismo_total - users_prismo_before
        evo_osada = osada - osada_before
        evo_users_osada = users_osada_total - users_osada_before
        evo_groundpolis = groundpolis - groundpolis_before
        evo_users_groundpolis = users_gpolis_total - users_groundpolis_before
        evo_ganggo = ganggo - ganggo_before
        evo_users_ganggo = users_ggg_total - users_ganggo_before
        evo_squs = squs - squs_before
        evo_users_squs = users_squs_total - users_squs_before
        evo_peertube = peertube - peertube_before
        evo_users_peertube = users_peertube_total - users_peertube_before
        evo_friendica = friendica - friendica_before
        evo_users_friendica = users_friendica_total - users_friendica_before
        evo_pixelfed = pixelfed - pixelfed_before
        evo_users_pixelfed = users_pixelfed_total - users_pixelfed_before

      except (Exception, psycopg2.DatabaseError) as error:

        print(error)

      finally:

        if conn is not None:

          conn.close()

      ################################################################################
      #  escriure en la bbdd evo 
      ################################################################################

      insert_line = """INSERT INTO evo(datetime, visited, mastodon, others, noresponse, sslerror, timeout_error, conerror, serv_total, users_total, pleroma, users_mastodon,
                       users_pleroma, gnusocial, users_gs, zap, users_zap, plume, users_plume, hubzilla, users_hubzilla, misskey, users_misskey, prismo, users_prismo, osada, users_osada,
                       groundpolis, users_groundpolis, ganggo, users_ganggo, squs, users_squs, peertube, users_peertube, friendica, users_friendica, pixelfed, users_pixelfed) 
                       VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING datetime;"""

      conn = None

      try:

        conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        # executem INSERT
        cur.execute(insert_line, (ara, evo_visited, evo_mastodont, evo_others, evo_noresponse, evo_sslerror, evo_timeout_error, evo_connerror, evo_serv_total, evo_users_total, evo_pleroma, evo_users_mast,
                                    evo_users_pl, evo_gnusocial, evo_users_gs, evo_zap, evo_users_zap, evo_plume, evo_users_plume, evo_hubzilla, evo_users_hub, evo_misskey, evo_users_misskey, 
                                    evo_prismo, evo_users_prismo, evo_osada, evo_users_osada, evo_groundpolis, evo_users_groundpolis, evo_ganggo, evo_users_ganggo, evo_squs, evo_users_squs, 
                                    evo_peertube, evo_users_peertube, evo_friendica, evo_users_friendica, evo_pixelfed, evo_users_pixelfed))

        # salvar els canvis en la base de dades
        conn.commit()

        # i tancar la connexió amb la base de dades
        cur.close()

      except (Exception, psycopg2.DatabaseError) as error:

        print(error)

      finally:

        if conn is not None:

          conn.close()

      ################################################################################

    #if i == 100:
    #if i == len(federated_servers):

      servers_total = mastodont + pleroma + gnusocial + zap + plume + hubzilla + misskey + prismo + osada + groundpolis + ganggo + squs + peertube + friendica + pixelfed + others
      visited = len(federated_servers)

      #######################################################################################################################################################################
      # Connectar amb la bbdd Postgres pggrafana per guardar les variables per grafana
      #

      insert_line = """INSERT INTO federated_servers(datetime, visited, mastodon, others, no_response, ssl_error, timeout_error, connection_error, total_servers, total_users, pleroma, masto_users, 
                       pleroma_users, gnusocial, gnusocial_users, zap, zap_users, plume, plume_users, hubzilla, hubzilla_users, misskey, misskey_users, prismo, prismo_users, osada, osada_users,
                       groundpolis, groundpolis_users, ganggo, ganggo_users, squs, squs_users, peertube, peertube_users, friendica, friendica_users, pixelfed, pixelfed_users) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                       %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING datetime;"""
      conn = None

      try:

        conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

        cur = conn.cursor()

        # executem INSERT
        cur.execute(insert_line, (ara, visited, mastodont, others, noresponse, sslerror, timeouterror, connectionerror, servers_total, users_total, pleroma, users_mast_total, users_pl_total, 
                              gnusocial, users_gs_total, zap, users_zap_total, plume, users_plume_total, hubzilla, users_hubzilla_total, misskey, users_misskey_total, prismo, users_prismo_total, 
                              osada, users_osada_total, groundpolis, users_gpolis_total, ganggo, users_ggg_total, squs, users_squs_total, peertube, users_peertube_total, friendica, 
                              users_friendica_total, pixelfed, users_pixelfed_total))

        # salvar els canvis en la base de dades
        conn.commit()

        # i tancar la connexió amb la base de dades
        cur.close()

      except (Exception, psycopg2.DatabaseError) as error:

        print(error)

      finally:

        if conn is not None:

          conn.close()

      ###############################################################################
      # calc percentages
      ###############################################################################

      print("Percentages:")
      print("Visited domains: " + str(visited))

      perc_no_response = round(((noresponse * 100.00) / visited), 2)
      print("No response domains: " + str(noresponse) + " (" + str(perc_no_response) + "%)")

      perc_error_ssl = round(((sslerror * 100.00) / visited), 2)
      print("SSL error: " + str(sslerror) + " (" + str(perc_error_ssl) + "%)")

      perc_error_tout = round(((timeouterror * 100.00) / visited), 2)       
      print("TimeOut error: " + str(timeouterror) + " (" + str(perc_error_tout) + "%)")

      perc_error_conn = round(((connectionerror * 100.00) / visited), 2)       
      print("Connection error: " + str(connectionerror) + " (" + str(perc_error_conn) + "%)")

      print("The fediverse:" + "\n")
      print("Active servers: " + str(servers_total))
      print("Registered users: " + str(users_total))

      ###############################################################################
      # T  O  O  T !
      ###############################################################################

      toot_text = "#fediverse view from https://" + mastodon_hostname + " \n"
      toot_text += "\n"
      toot_text += "Domains:" + "\n"
      toot_text += "Visited " + str(visited) + " \n"
      toot_text += "No response " + str(noresponse)  + " (" + str(perc_no_response) + "%)" + "\n"
      toot_text += "SSL error " + str(sslerror)  + " (" + str(perc_error_ssl) + "%)" + "\n"
      toot_text += "Timeout error " + str(timeouterror)  + " (" + str(perc_error_tout) + "%)" + "\n"
      toot_text += "Connection error " + str(connectionerror)  + " (" + str(perc_error_conn) + "%)" + "\n"
      toot_text += "\n"
      toot_text += "The fediverse:" + "\n"
      toot_text += "Total active servers: " + str(servers_total) + "\n"
      toot_text += "Mastodon -> " + str(mastodont) + "\n"
      toot_text += "Pleroma -> " + str(pleroma) + "\n"
      toot_text += "Peertube -> " + str(peertube) + "\n"
      toot_text += "\n"
      toot_text += "Total registered users: " + str(users_total) + "\n"
      toot_text += "Mastodon -> " + str(users_mast_total) + "\n"
      toot_text += "Pleroma: -> " + str(users_pl_total) + "\n"
      toot_text += "Peertube -> " + str(users_peertube_total) + "\n"
      toot_text += "\n"

      print("Tooting...")
      print(toot_text)

      mastodon.status_post(toot_text, in_reply_to_id=None, )

      print("Tooted succesfully!")

