#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pdb
import getpass
import os
import sys
from mastodon import Mastodon
from mastodon.Mastodon import MastodonMalformedEventError, MastodonNetworkError, MastodonReadTimeout, MastodonAPIError 
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, asking."%file_path)
        write_parameter( parameter, file_path )
        #sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

def write_parameter( parameter, file_path ):
  print("Setting up host parameters...")
  print("\n")
  mastodon_hostname = input("Enter Mastodon hostname: ")
  mastodon_db = input("Enter Mastodon database name: ")
  mastodon_db_user = input("Enter Mastodon database user: ")
  fediverse_db = input("Fediverse db name: ")
  fediverse_db_user = input("Fediverse db user: ")
  with open(file_path, "w") as text_file:
    print("mastodon_hostname: {}".format(mastodon_hostname), file=text_file)
    print("mastodon_db: {}".format(mastodon_db), file=text_file)
    print("mastodon_db_user: {}".format(mastodon_db_user), file=text_file)
    print("fediverse_db: {}".format(fediverse_db), file=text_file)
    print("fediverse_db_user: {}".format(fediverse_db_user), file=text_file)

def create_table(db, db_user, table, sql):

  try:

    conn = None
    conn = psycopg2.connect(database = db, user = db_user, password = "", host = "/var/run/postgresql", port = "5432")
    cur = conn.cursor()


    print("Creating table "+table)
    # Create the table in PostgreSQL database
    cur.execute(sql)

    conn.commit()
    print("Table "+table+" created!")
    print("\n")

  except (Exception, psycopg2.DatabaseError) as error:

    print(error)

  finally:

    if conn is not None:

      conn.close()

#############################################################################################
  
# Load configuration from config file
config_filepath = "config.txt"
mastodon_hostname = get_parameter("mastodon_hostname", config_filepath) # E.g., mastodon.site
mastodon_db = get_parameter("mastodon_db", config_filepath) # E.g., mastodon_production
mastodon_db_user = get_parameter("mastodon_db_user", config_filepath) # E.g., mastodon
fediverse_db = get_parameter("fediverse_db", config_filepath) # E.g., fediverse_prod
fediverse_db_user = get_parameter("fediverse_db_user", config_filepath) # E.g., mastodon

#############################################################################################

try:

  conn = None
  conn = psycopg2.connect(database = mastodon_db, user = mastodon_db_user, password = "", host = "/var/run/postgresql", port = "5432")
 
except (Exception, psycopg2.DatabaseError) as error:

  print(error)
  # Load configuration from config file
  os.remove("config.txt")
  print("Exiting. Run setup again with right parameters")
  sys.exit(0)

############################################################
# create database
############################################################

try:

  conn = psycopg2.connect(dbname='postgres',
      user=fediverse_db_user, host='',
      password='')

  conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

  cur = conn.cursor()

  cur.execute(sql.SQL("CREATE DATABASE {}").format(
          sql.Identifier(fediverse_db))
      )

except (Exception, psycopg2.DatabaseError) as error:

  print(error)

finally:

  if conn is not None:

    conn.close()

#############################################################################################

try:

  conn = None
  conn = psycopg2.connect(database = fediverse_db, user = fediverse_db_user, password = "", host = "/var/run/postgresql", port = "5432")

except (Exception, psycopg2.DatabaseError) as error:

  print(error)
  # Load configuration from config file
  os.remove("config.txt")
  print("Exiting. Run setup again with right parameters")
  sys.exit(0)

if conn is not None:

  print("\n")
  print("Host parameters saved to config.txt!")
  print("\n")

############################################################
# Create needed tables 
############################################################

print("Creating tables...")

########################################

db = fediverse_db
db_user = fediverse_db_user
table = "federation"
sql = "create table "+table+" (server varchar(30) PRIMARY KEY, users int, fed_servers INT, toots INT, updated_at timestamptz, software varchar(10))"

create_table(db, db_user, table, sql)

#########################################

db = fediverse_db
db_user = fediverse_db_user
table = "conn_error_servers"
sql = "create table "+table+" (server varchar(30) PRIMARY KEY, added timestamptz, updated_at timestamptz, days varchar(30))"

create_table(db, db_user, table, sql)

#########################################

db = fediverse_db
db_user = fediverse_db_user
table =	"federated_servers"
sql = "create table "+table+" (datetime timestamptz PRIMARY KEY, visited INT, mastodon INT, masto_users INT, pleroma INT, pleroma_users INT, peertube INT, peertube_users INT, pixelfed INT,"
sql += "pixelfed_users INT, gnusocial INT, gnusocial_users INT, hubzilla INT, hubzilla_users INT, plume INT, plume_users INT, prismo INT, prismo_users INT, zap INT, zap_users INT, osada INT,"
sql += "osada_users INT, groundpolis INT, groundpolis_users INT, squs INT, squs_users INT, ganggo INT, ganggo_users INT, friendica INT, friendica_users INT, misskey INT, misskey_users INT,"
sql += "others INT, total_servers INT, total_users INT, no_response INT, ssl_error INT, timeout_error INT, connection_error INT)"

create_table(db, db_user, table, sql)

########################################

db = fediverse_db
db_user = fediverse_db_user
table = "no_response_servers"
sql = "create table "+table+" (server varchar(30) PRIMARY KEY, added timestamptz, updated_at timestamptz, days varchar(30))"

create_table(db, db_user, table, sql)

#######################################

db = fediverse_db
db_user = fediverse_db_user
table = "ssl_error_servers"
sql = "create table "+table+" (server varchar(30) PRIMARY KEY, added timestamptz, updated_at timestamptz, days varchar(30))"

create_table(db, db_user, table, sql)

######################################

db = fediverse_db
db_user = fediverse_db_user
table = "time_out_servers"
sql = "create table "+table+" (server varchar(30) PRIMARY KEY, added timestamptz, updated_at timestamptz, days varchar(30))"

create_table(db, db_user, table, sql)

######################################

db = fediverse_db
db_user = fediverse_db_user
table = "evo"
sql = "create table "+table+" (datetime timestamptz PRIMARY KEY, visited INT, mastodon INT, others INT, noresponse INT, sslerror INT, timeout_error INT, conerror INT, serv_total INT,"
sql += "users_total INT, pleroma INT, users_mastodon INT, users_pleroma INT, gnusocial INT, users_gs INT, zap INT, users_zap INT, plume INT, users_plume INT, hubzilla INT,"
sql += "users_hubzilla INT, misskey INT, users_misskey INT, prismo INT, users_prismo INT, osada INT, users_osada INT, groundpolis INT, users_groundpolis INT, ganggo INT,"
sql += "users_ganggo INT, squs INT, users_squs INT, peertube INT, users_peertube INT, friendica INT, users_friendica INT, pixelfed INT, users_pixelfed INT)"

create_table(db, db_user, table, sql)

#####################################

print("Done!")
print("Now you can run fediverse.py!")
print("\n")
