# mastofed
Your Mastodon server's known fediverse

You know how many servers are federated with your Mastodon server but don't know much about them. What software are they running? How many users? Are they still alive?
These Python scripts will help you to get all that information and much more! 

### Dependencies

-   **Python 3**
-   Postgresql server
-   Everything else at the top of `fediverse.py`!

### Usage:

Within Python Virtual Environment:

1. Run 'db-setup.py' to create needed database and tables. All collected data will be written there. You can use software like Grafana to visualize your fediverse metrics!

2. Run 'setup.py' to get your Mastodon's access token of an existing user. It will be saved to 'secrets/secrets.txt' for further use.

3. Run 'fediverse.py' to start collecting all information about your Mastodon server's federated servers, their stats, if they are alive etc. 

Once it finished, will post some of collected stats to your Mastodon server!

Note: install all needed packages with 'pip install package'

![Grafana metrics sample](grafana_metric_sample.png)
